#!/bin/bash

if ! grep -q "splash" /etc/default/grub; then
    eche "Updating grub config."
    sed -i '/^GRUB_CMDLINE_LINUX=/ s/"$/ splash"/' /etc/default/grub

    grub-mkconfig -o /boot/grub/grub.cfg
fi

if ! grep -q "plymouth" /etc/mkinitcpio.conf; then
    if ! grep -q '^HOOKS=.*filesystems' /etc/mkinitcpio.conf; then
        echo "Error: 'filesystems' not found in mkinitcpio.conf."
        exit 1
    fi
    eche "Updating mkinitcpio config."
    sed -i '/^HOOKS=/ s/filesystems/plymouth filesystems/' /etc/mkinitcpio.conf
fi

script_path="$(cd "$(dirname "$0")" && pwd)"
theme_dir="$script_path/assets/hexagon_dots_alt"

sudo cp -r $theme_dir /usr/share/plymouth/themes/

sudo plymouth-set-default-theme -R hexagon_dots_alt
