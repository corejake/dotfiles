#!/bin/bash

# Set the paths and days threshold
days_threshold="30"

# Get the current user
current_user="core"
pwd="/home/"$current_user

# Create a temporary file for the new crontab entries
temp_cron_file=$(mktemp)

# Write the crontab entries to the temporary file
echo "0 2 * * 0 $pwd/dotfiles/delete_unused.sh $pwd/Downloads $days_threshold" >> "$temp_cron_file"
echo "0 2 * * 0 $pwd/dotfiles/delete_unused.sh $pwd/gitclones $days_threshold" >> "$temp_cron_file"

# Install the new crontab
sudo crontab -u "$current_user" "$temp_cron_file"

# Clean up the temporary file
rm "$temp_cron_file"
