#!/bin/bash
printf 'script started' | systemd-cat -t check-battery
BATTINFO=`acpi -b`
if [[ `echo $BATTINFO | grep Discharging` && `echo $BATTINFO | cut -f 5 -d " "` < 00:05:00 ]] ; then
        DISPLAY=:0 /usr/bin/notify-send -u low "battery" "$BATTINFO"
fi
