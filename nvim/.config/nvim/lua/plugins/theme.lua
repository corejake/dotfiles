--require('onedark').setup()
require('lualine').setup()

vim.cmd.colorscheme('nightfox')
vim.opt.conceallevel = 2
