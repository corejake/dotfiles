require('nvim-dap-virtual-text').setup()
require('dapui').setup()

vim.keymap.set("n", "<leader>dt", require('dapui').toggle)
vim.keymap.set("n", "<leader>db", "<cmd>DapToggleBreakpoint<cr>")
vim.keymap.set("n", "<leader>dc", "<cmd>DapContinue<cr>")
vim.keymap.set("n", "<leader>do", "<cmd>DapStepOver<cr>")
vim.keymap.set("n", "<leader>di", "<cmd>DapStepInto<cr>")
vim.keymap.set("n", "<leader>dO", "<cmd>DapStepOut<cr>")
