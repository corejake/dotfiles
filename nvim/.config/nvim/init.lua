require("remap")
require("set")

require("plugins.lazy")

require("plugins.theme")

require("plugins.telescope")
require("plugins.treesitter")
require("plugins.undotree")

require("plugins.ufo")
require("plugins.lsp")

require("plugins.dap")
require("plugins.gitsigns")
require("plugins.fugitive")
require("plugins.harpoon")
