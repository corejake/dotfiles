#!/bin/bash

#theme to install
package_name="sddm-theme-tokyo-night"

function get_pm() {
    if command -v paru &> /dev/null; then
        echo "paru"
    elif command -v yay &> /dev/null;then
        echo "yay"
    else
        echo "Error: Neither paru nor yay is installed. Exiting."
        exit 1
    fi
}

#enable sddm service
if systemctl is-enabled sddm &> /dev/null; then
    echo "sddm is already enabled. Skipping enabling."
else
    sudo systemctl enable sddm
    echo "sddm has been enabled."
fi

if pacman -Qs "$package_name" > /dev/null; then
    echo "$package_name is already installed. Skipping installation."
else
    "$(get_pm)" -S $package_name
fi

#set theme in configs
file_path="/usr/lib/sddm/sddm.conf.d/default.conf"
default_file_path="/etc/sddm.conf"

update_sddm_theme() {
    local file_path=$1

    if [ ! -f "$file_path" ]; then
        echo "File not found: \"$file_path\"\n Config file not modified"
        return
    fi

    sudo sed -i 's/^Current=.*/Current=tokyo-night-sddm/' "$file_path"
}

update_sddm_theme "$file_path"
update_sddm_theme "$default_file_path"


#change backgrond
script_path="$(cd "$(dirname "$0")" && pwd)"
image_path="$script_path/assets/hotline.jpg"
sudo cp $image_path /usr/share/sddm/themes/tokyo-night-sddm/Backgrounds/hotline.jpg

#edit theme settings
theme_config_path="/usr/share/sddm/themes/tokyo-night-sddm/theme.conf"

if [ -f "$theme_config_path" ]; then
    sudo sed -i 's#^Background=.*#Background="Backgrounds/hotline.jpg"#' "$theme_config_path"
    sudo sed -i 's#^PartialBlur=.*#PartialBlur="true"#' "$theme_config_path"
    sudo sed -i 's#^BlurRadius=.*#BlurRadius="10"#' "$theme_config_path"
else
    echo "File not found: $theme_config_path"
fi

