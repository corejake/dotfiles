#!/bin/bash

# Set the directory you want to search
search_directory=$1

# Set the number of days for which the files haven't been accessed
days_threshold=$2

# Use the find command to search for files not accessed in the last N days
find $search_directory -atime +$days_threshold -delete
