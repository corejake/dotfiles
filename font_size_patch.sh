#!/bin/bash

patch_alacritty_config() {
    local config_path="alacritty/.config/alacritty.yml"

    # Patch the font size in the alacritty config file
    sed -i "/font:/{:a;N;/size: [0-9]*/s/size: [0-9]*/size: $FONT_SIZE/;ba}" "$config_path"
}

# Get screen width, height, and DPI
SCREEN_WIDTH=$(xdpyinfo | awk '/dimensions:/ {print $2}' | cut -d 'x' -f1)
SCREEN_HEIGHT=$(xdpyinfo | awk '/dimensions:/ {print $2}' | cut -d 'x' -f2)
DPI=$(xdpyinfo | awk '/resolution:/ {print $2}' | awk -Fx '{print $1}')

# Rules for font size choosing
if [ "$DPI" -ge 96 ]; then
    MULTIPLIER=0.55
else
    MULTIPLIER=0.3
fi

# Calculate font size
FONT_SIZE=$(echo "scale=2; $SCREEN_WIDTH * $MULTIPLIER / 100" | bc)
FONT_SIZE=${FONT_SIZE%.*}

# Set font size in your terminal emulator config file
echo "Font_Size=$FONT_SIZE" 
#>> ~/.terminal_config

#Alacritty font size patch
patch_alacritty_config

# Apply the config
# Your command to apply the config goes here

echo "Font size set based on screen size and DPI."
