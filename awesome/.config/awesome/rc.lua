--- ░█▀▄░█░█░█░█░█░█░█▀█░▀░█▀▀░░░█▀█░█░█░█▀▀░█▀▀░█▀█░█▄█░█▀▀
--- ░█▀▄░▄▀▄░░█░░█▀█░█░█░░░▀▀█░░░█▀█░█▄█░█▀▀░▀▀█░█░█░█░█░█▀▀
--- ░▀░▀░▀░▀░░▀░░▀░▀░▀░▀░░░▀▀▀░░░▀░▀░▀░▀░▀▀▀░▀▀▀░▀▀▀░▀░▀░▀▀▀
--- ~~~~~~~~~~~~~~~~~~  @author rxyhn ~~~~~~~~~~~~~~~~~~~~~~
--- ~~~~~~~~~~~~ https://github.com/rxyhn ~~~~~~~~~~~~~~~~~~

pcall(require, "luarocks.loader")
local gears = require("gears")
local beautiful = require("beautiful")

--- ░▀█▀░█░█░█▀▀░█▄█░█▀▀
--- ░░█░░█▀█░█▀▀░█░█░█▀▀
--- ░░▀░░▀░▀░▀▀▀░▀░▀░▀▀▀

local theme_dir = gears.filesystem.get_configuration_dir() .. "theme/"
beautiful.init(theme_dir .. "theme.lua")

--- ░█▀▀░█▀█░█▀█░█▀▀░▀█▀░█▀▀░█░█░█▀▄░█▀█░▀█▀░▀█▀░█▀█░█▀█░█▀▀
--- ░█░░░█░█░█░█░█▀▀░░█░░█░█░█░█░█▀▄░█▀█░░█░░░█░░█░█░█░█░▀▀█
--- ░▀▀▀░▀▀▀░▀░▀░▀░░░▀▀▀░▀▀▀░▀▀▀░▀░▀░▀░▀░░▀░░▀▀▀░▀▀▀░▀░▀░▀▀▀

require("configuration")

--- ░█▄█░█▀█░█▀▄░█░█░█░░░█▀▀░█▀▀
--- ░█░█░█░█░█░█░█░█░█░░░█▀▀░▀▀█
--- ░▀░▀░▀▀▀░▀▀░░▀▀▀░▀▀▀░▀▀▀░▀▀▀

require("modules")

local klayout = require("utilities.kbdcfg").kbdcfg
local kbdcfg = klayout({type = "tui", remember_layout = true})

kbdcfg.add_primary_layout("English", "EN", "us")
kbdcfg.add_primary_layout("Halmak", "HK", "halmak_no_qwerty")
kbdcfg.add_primary_layout("Russian", "RU", "ru")

kbdcfg.bind("alt_shift_toggle")

--- ░█░█░▀█▀
--- ░█░█░░█░
--- ░▀▀▀░▀▀▀

require("ui")

--- ░█▀▀░█▀█░█▀▄░█▀▄░█▀█░█▀▀░█▀▀
--- ░█░█░█▀█░█▀▄░█▀▄░█▀█░█░█░█▀▀
--- ░▀▀▀░▀░▀░▀░▀░▀▀░░▀░▀░▀▀▀░▀▀▀

--- Enable for lower memory consumption
collectgarbage("setpause", 110)
collectgarbage("setstepmul", 1000)
gears.timer({
	timeout = 5,
	autostart = true,
	call_now = true,
	callback = function()
		collectgarbage("collect")
	end,
})

local awful = require("awful")
client.connect_signal("request::default_keybindings", function()
    awful.keyboard.append_client_keybindings({
        -- Shift-Alt to change keyboard layout
        awful.key({"Shift"}, "Alt_L", function () kbdcfg.update_current() end),
        -- Alt-Shift to change keyboard layout
        awful.key({"Mod1"}, "Shift_L", function () kbdcfg.update_current() end)
    })
end)

--- Propagate desktop notification to the phone
local naughty = require("naughty")
naughty.connect_signal("new", function(args)
    -- Get the notification details
    local title = args.title or ""
    local text = args.text or ""

    -- Escape single quotes to avoid breaking the command
    title = title:gsub("'", "'\\''")
    text = text:gsub("'", "'\\''")

    ---if args.app_name == "Zulip" then
        local awful = require("awful")
        local command = string.format("send_to_ntfy '%s' '%s|%s'", title, app_name, text)

        -- Execute the command
        awful.spawn.with_shell(command)
    ---end
end
)
