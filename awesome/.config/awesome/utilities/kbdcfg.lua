local awful = require("awful")
local wibox = require("wibox")
local kbdcfg = {}

local function get_current_layout_index()
    local handle = io.popen("xkblayout-state print %c")
    if handle ~= nil then
        local result = handle:read("*a")
        handle:close()
        return tonumber(result)
    end
    return 0
end

-- Function to update current layout
function kbdcfg.update_current()
    local current_index = get_current_layout_index()
    --print("current layout index: " .. current_index)
    kbdcfg.current = current_index + 1

    if client.focus then
        local layout = kbdcfg.additional_layouts[kbdcfg.current]
        --print("focus set to " .. layout.name)
        client.focus.kbd_layout = layout.name
    end
end

-- Function to find layout in list and set current index
local function find_current_layout(name)
    for index, layout in ipairs(kbdcfg.additional_layouts) do
        if layout.name == name then
            return index, layout
        end
    end
    return nil, nil
end

-- Function to change current layout based on the name
function kbdcfg.switch_by_name(name)
    local _, layout = find_current_layout(name)
    kbdcfg.switch(layout)
end

-- Function to change layout
function kbdcfg.switch(layout)
    -- Find the current layout index
    local index = find_current_layout(layout.name)
    local move_index = index - kbdcfg.current
    kbdcfg.current = index

    -- Update the keyboard layout for the focused client if available
    if client.focus then
        local current_layout = kbdcfg.additional_layouts[kbdcfg.current]
        client.focus.kbd_layout = current_layout.name
    end

    -- Update the widget based on the layout type
    if kbdcfg.type == "tui" then
        kbdcfg.widget:set_text(kbdcfg.tui_wrap_left .. layout.label .. kbdcfg.tui_wrap_right)
    else
        kbdcfg.widget.image = layout.label
    end

    -- Change the keyboard layout state if needed
    if move_index ~= 0 then
        local sign = move_index > 0 and "+" or "-"
        os.execute("xkblayout-state set " .. sign .. math.abs(move_index))
    end
end

-- Function to add primary layouts
function kbdcfg.add_primary_layout(name, label, subcmd)
    local layout = { name   = name,
                     label  = label,
                     subcmd = subcmd };

    table.insert(kbdcfg.layouts, layout)
    table.insert(kbdcfg.additional_layouts, layout)
end

-- Function to add additional layouts
function kbdcfg.add_additional_layout(name, label, subcmd)
    local layout = { name   = name,
                     label  = label,
                     subcmd = subcmd };

    table.insert(kbdcfg.additional_layouts, layout)
end

-- Bind function. Applies all settings to the widget
function kbdcfg.bind(switch_variant)
    -- Menu for choose additional keyboard layouts
    local menu_items = {}

    for i = 1, #kbdcfg.additional_layouts do
        local layout = kbdcfg.additional_layouts[i]
        table.insert(menu_items, {
                         layout.name,
                         function () kbdcfg.switch(layout) end,
                         -- show a fancy image in gui mode
                         kbdcfg.type == "gui" and layout.label or nil
        })
    end

    kbdcfg.menu = awful.menu({ items = menu_items })

    if kbdcfg.type == "tui" then
        kbdcfg.widget = wibox.widget.textbox()
    else
        kbdcfg.widget = wibox.widget.imagebox()
    end

    if kbdcfg.default_layout_index > #kbdcfg.layouts then
        kbdcfg.default_layout_index = 1;
        kbdcfg.current = kbdcfg.default_layout_index;
    end

    if kbdcfg.remember_layout then
        client.connect_signal("focus", Kbd_client_update)
    end

    --local current_layout = kbdcfg.layouts[kbdcfg.current]

    local command = "setxkbmap "
    for i, layout in ipairs(kbdcfg.layouts) do
        if i > 1 then
            command = command .. ","
        end
        command = command .. layout.subcmd
    end
    command = command .. " -option grp:" .. switch_variant

    os.execute(command)
end

-- Callback function for set remembering layout for windows
function Kbd_client_update(c)
    if not c then
        return
    end

    if not c.kbd_layout then
        c.kbd_layout = kbdcfg.layouts[kbdcfg.current].name
    end
    --print("kbd update " .. c.kbd_layout)

    kbdcfg.switch_by_name(c.kbd_layout)
end

local function get_current_layout()
    if kbdcfg.layouts == nil then
        return "Nil"
    end
    return kbdcfg.layouts[kbdcfg.current].name
end

-- Factory function. Creates the widget.
local function factory(args)
    local args                   = args or {}
    kbdcfg.cmd                   = args.cmd or "setxkbmap"
    kbdcfg.layouts               = args.layouts or {}
    kbdcfg.additional_layouts    = args.additional_layouts or {}
    kbdcfg.default_layout_index  = args.default_layout_index or 1
    kbdcfg.current               = args.current or kbdcfg.default_layout_index
    kbdcfg.menu                  = nil
    kbdcfg.type                  = args.type or "tui"
    kbdcfg.tui_wrap_left         = args.tui_wrap_left  or " "
    kbdcfg.tui_wrap_right        = args.tui_wrap_right or " "
    kbdcfg.remember_layout       = args.remember_layout or false

    for i = 1, #kbdcfg.layouts do
        table.insert(kbdcfg.additional_layouts, kbdcfg.layouts[i])
    end

    return kbdcfg
end

setmetatable(kbdcfg, { __call = function(_, ...) return factory(...) end })

return {kbdcfg = kbdcfg, get_current_layout = get_current_layout}
