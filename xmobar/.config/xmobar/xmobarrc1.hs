Config
{ font    =      "xft:Hack Nerd Font:pixelsize=18:antialias=true:hinting=true"
, bgColor =      "black"
, fgColor =      "white"
, border  =       BottomB
, borderColor =  "black"
, allDesktops =  True
, commands = -- what information to show
  -- Network activity monitor (dynamic interface resolution)
  [ Run DynNetwork ["-t", " <rx>kb|| <tx>kb","--Low","100000","--High","500000","--low","green","--normal","yellow","--high","red"] 15 -- network traffic "enp0s31f6"
  -- Keyboard layout
 , Run Kbd [ ("halmak_no_qwerty","<fc=#2820da>ﴑ HL</fc>") 
	   , ("us","<fc=#20daa9> US</fc>")
	   , ("ru","<fc=#20a2da> RU</fc>")
	   ] --"halmak", "<fc=#00001B>HAL</fc>"
 -- , Run Volume "default" "Master" [] 20
  , Run Date " %d %b %T" "mydate" 10
  , Run UnsafeStdinReader
  ]
-- where to show command information
, template = "%UnsafeStdinReader% }{%kbd% | %dynnetwork% | %mydate% |"
}
