#version 450

#extension GL_ARB_separate_shader_objects: enable

#define E  2.71828182
#define PI 3.14159265

int B[]=
int[9]   (0,4096,65600,332772,15255086,23385164,15252014,13199452,11512810);


layout (binding = 0) uniform UniformBufferObject {
    vec2 iResolution;
};

layout( push_constant ) uniform constants
{
    float time;
};

layout (location = 0) out vec4 outColor;

void main() {
    const vec2 R = iResolution;
    vec2 FragCoord = vec2(gl_FragCoord.x, R.y - gl_FragCoord.y);
        vec2
    Z   = vec2(R.x/7.),
    sc  = vec2(1,R.y/R.x)*Z.x,
    p   = (FragCoord.xy-R*.5)*sc.xy/R,
    p1  = floor(p)/sc; p=floor(fract(p)*7.);
    
    float q = length(p1), I=pow(sin(time*.1-q*10.), 4.);
    I = mod(log(q)/log(E*5.)+atan(p1.x, p1.y)/(2.*PI)-time*.02, .5)*2.;
    I = (smoothstep(1./8., 0.7, I)-smoothstep(0.5, 1., I))*1.5;
    I = min(I, smoothstep(0., 0.2, pow(sin(time*.1-q*10.), 4.)));
    
    outColor =
    vec4(
            float((B[min(int(I*8.), 8)] >> int(p.x + p.y*5.)) & 1)
            *step(0.,p.x)*step(0.,p.y)*(1.-step(5.,p.x))*(1.-step(5.,p.y))
            );
    }
